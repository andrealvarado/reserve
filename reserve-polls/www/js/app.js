// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('ReserveApp', ['ionic', 'ReserveApp.controllers', 'ReserveApp.factory'])
    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });
    }).run(['$rootScope', 'AuthFactory',
        function ($rootScope, AuthFactory) {

            $rootScope.isAuthenticated = AuthFactory.isLoggedIn();

            // utility method to convert number to an array of elements
            $rootScope.getNumber = function (num) {
                return new Array(num);
            }

        }
    ]).config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
        function ($stateProvider, $urlRouterProvider, $httpProvider) {

            // setup the token interceptor
            $httpProvider.interceptors.push('TokenInterceptor');

            $stateProvider

                .state('app', {
                    url: "/app",
                    abstract: true,
                    templateUrl: "templates/menu.html",
                    controller: 'AppCtrl'
                })

                .state('app.browse', {
                    url: "/browse",
                    views: {
                        'menuContent': {
                            templateUrl: "templates/browse.html",
                            controller: 'BrowseCtrl'
                        }
                    }
                })

                .state('app.restaurant', {
                    url: "/restaurant/:restaurantId",
                    views: {
                        'menuContent': {
                            templateUrl: "templates/restaurant.html",
                            controller: 'RestaurantCtrl'
                        }
                    }
                })
                
                .state('app.restaurant_data_reserva', {
                    url: "/restaurant_data_reserva/:restaurantId",
                    views: {
                        'menuContent': {
                            templateUrl: "templates/restaurant_data_reserva.html",
                            controller: 'RestaurantDataReservaCtrl'
                        }
                    }
                })
                .state('app.restaurant_horario_reserva', {
                    url: "/restaurant_horario_reserva/:restaurantId",
                    views: {
                        'menuContent': {
                            templateUrl: "templates/restaurant_data_reserva.html",
                            controller: 'RestaurantHorarioReservaCtrl'
                        }
                    }
                })

                .state('app.minhas_reservas', {
                    url: "/minhas_reservas",
                    views: {
                        'menuContent': {
                            templateUrl: "templates/minhas_reservas.html",
                            controller: 'MinhasReservasCtrl'
                        }
                    }
                })

            // if none of the above states are matched, use this as the fallback
            $urlRouterProvider.otherwise('/app/browse');
        }
    ])

