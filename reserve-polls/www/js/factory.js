
var base2 = 'http://localhost:8000';
var base = 'https://ionic-book-store.herokuapp.com';

angular.module('ReserveApp.factory', [])

    .factory('Loader', ['$ionicLoading', '$timeout', function ($ionicLoading, $timeout) {

        var LOADERAPI = {

            showLoading: function (text) {
                text = text || 'Loading...';
                $ionicLoading.show({
                    template: text
                });
            },

            hideLoading: function () {
                $ionicLoading.hide();
            },

            toggleLoadingWithMessage: function (text, timeout) {
                var self = this;

                self.showLoading(text);

                $timeout(function () {
                    self.hideLoading();
                }, timeout || 3000);
            }

        };
        return LOADERAPI;
    }])

    .factory('LSFactory', [function () {

        var LSAPI = {

            clear: function () {
                return localStorage.clear();
            },

            get: function (key) {
                return JSON.parse(localStorage.getItem(key));
            },

            set: function (key, data) {
                return localStorage.setItem(key, JSON.stringify(data));
            },

            delete: function (key) {
                return localStorage.removeItem(key);
            },

            getAll: function () {
                var restaurantes = [];
                var items = Object.keys(localStorage);

                for (var i = 0; i < items.length; i++) {
                    if (items[i] !== 'user' || items[i] != 'token') {
                        restaurantes.push(JSON.parse(localStorage[items[i]]));
                    }
                }

                return restaurantes;
            }

        };

        return LSAPI;

    }])


    .factory('AuthFactory', ['LSFactory', function (LSFactory) {

        var userKey = 'user';
        var tokenKey = 'token';

        var AuthAPI = {

            isLoggedIn: function () {
                return this.getUser() === null ? false : true;
            },

            getUser: function () {
                return LSFactory.get(userKey);
            },

            setUser: function (user) {
                return LSFactory.set(userKey, user);
            },

            getToken: function () {
                return LSFactory.get(tokenKey);
            },

            setToken: function (token) {
                return LSFactory.set(tokenKey, token);
            },

            deleteAuth: function () {
                LSFactory.delete(userKey);
                LSFactory.delete(tokenKey);
            }

        };

        return AuthAPI;

    }])

    .factory('TokenInterceptor', ['$q', 'AuthFactory', function ($q, AuthFactory) {

        return {
            request: function (config) {
                config.headers = config.headers || {};
                var token = AuthFactory.getToken();
                var user = AuthFactory.getUser();

                if (token && user) {
                    config.headers['X-Access-Token'] = token.token;
                    config.headers['X-Key'] = user.email;
                    config.headers['Content-Type'] = "application/json";
                }
                return config || $q.when(config);
            },

            response: function (response) {
                return response || $q.when(response);
            }
        };

    }])


    .factory('RestaurantsFactory', ['$http', function ($http, $httpParamSerializerJQLike) {
        var API = {
            get: function () {
                return $http.get(base2 + '/polls/restaurantes/');
            }, 

            getHorarios: function (myData) {
                //return $http({
                  //  url: base2 + '/polls/horarios/',
                    //method: 'POST',
                   // data: {}
               // });
               $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
               return $http({
                            url: base2 + '/polls/horarios/',
                            method: "POST",
                            data: myData
                        })
                        .then(function(response) {
                            console.log("Funcionou")
                        }, 
                        function(response) { // optional
                            console.log("Falhou")
                                // failed
                        }); 
            }
        };

        return API;
    }])

    .factory('UserFactory', ['$http', 'AuthFactory',
        function ($http, AuthFactory) {

            var UserAPI = {

                login: function (user) {
                    return $http.post(base + '/login', user);
                },

                register: function (user) {
                    return $http.post(base + '/register', user);
                },

                logout: function () {
                    AuthFactory.deleteAuth();
                },

                getCartItems: function () {
                    var userId = AuthFactory.getUser()._id;
                    return $http.get(base + '/api/v1/users/' + userId + '/cart');
                },

                addToCart: function (book) {
                    var userId = AuthFactory.getUser()._id;
                    return $http.post(base + '/api/v1/users/' + userId + '/cart', book);
                },

            };

            return UserAPI;
        }
    ])
