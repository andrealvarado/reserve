angular.module('ReserveApp.controllers', [])

    .controller('AppCtrl', ['$rootScope', '$ionicModal', 'AuthFactory', '$location', 'UserFactory', '$scope', 'Loader',
        function ($rootScope, $ionicModal, AuthFactory, $location, UserFactory, $scope, Loader) {

            $rootScope.$on('showLoginModal', function ($event, scope, cancelCallback, callback) {
                $scope.user = {
                    email: '',
                    password: ''
                };

                $scope = scope || $scope;

                $scope.viewLogin = true;

                $ionicModal.fromTemplateUrl('templates/login.html', {
                    scope: $scope
                }).then(function (modal) {
                    $scope.modal = modal;
                    $scope.modal.show();

                    $scope.switchTab = function (tab) {
                        if (tab === 'login') {
                            $scope.viewLogin = true;
                        } else {
                            $scope.viewLogin = false;
                        }
                    }

                    $scope.hide = function () {
                        $scope.modal.hide();
                        if (typeof cancelCallback === 'function') {
                            cancelCallback();
                        }
                    }

                    $scope.login = function () {
                        Loader.showLoading('Authenticating...');

                        UserFactory.login($scope.user).success(function (data) {

                            data = data.data;
                            AuthFactory.setUser(data.user);
                            AuthFactory.setToken({
                                token: data.token,
                                expires: data.expires
                            });

                            $rootScope.isAuthenticated = true;
                            $scope.modal.hide();
                            Loader.hideLoading();
                            if (typeof callback === 'function') {
                                callback();
                            }
                        }).error(function (err, statusCode) {
                            Loader.hideLoading();
                            Loader.toggleLoadingWithMessage(err.message);
                        });
                    }

                    $scope.register = function () {
                        Loader.showLoading('Registering...');

                        UserFactory.register($scope.user).success(function (data) {

                            data = data.data;
                            AuthFactory.setUser(data.user);
                            AuthFactory.setToken({
                                token: data.token,
                                expires: data.expires
                            });

                            $rootScope.isAuthenticated = true;
                            Loader.hideLoading();
                            $scope.modal.hide();
                            if (typeof callback === 'function') {
                                callback();
                            }
                        }).error(function (err, statusCode) {
                            Loader.hideLoading();
                            Loader.toggleLoadingWithMessage(err.message);
                        });
                    }
                });
            });

            $rootScope.loginFromMenu = function () {
                $rootScope.$broadcast('showLoginModal', $scope, null, null);
            }

            $rootScope.logout = function () {
                UserFactory.logout();
                $rootScope.isAuthenticated = false;
                $location.path('/app/browse');
                Loader.toggleLoadingWithMessage('Successfully Logged Out!', 2000);
            }


        }
    ])

    .controller('BrowseCtrl', ['$scope', 'RestaurantsFactory', 'LSFactory', 'Loader',
        function ($scope, RestaurantsFactory, LSFactory, Loader) {

            Loader.showLoading();

            // support for pagination

            $scope.restaurants = [];
            var restaurants = LSFactory.getAll();

            // if restaurants exists in localStorage, use that instead of making a call
            if (restaurants.length > 0) {
                $scope.restaurants = restaurants;
                Loader.hideLoading();
            } else {
                RestaurantsFactory.get().success(function (restaurantes) {
                    // process books and store them 
                    // in localStorage so we can work with them later on, 
                    // when the user is offline
                    console.log(restaurantes[0].nome)
                    processRestaurants(restaurantes);
                    $scope.restaurants = restaurantes;
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                    Loader.hideLoading();
                }).error(function (err, statusCode) {
                    Loader.hideLoading();
                    Loader.toggleLoadingWithMessage(err.message);
                });
            }

            function processRestaurants(restaurantes) {
                LSFactory.clear();
                // we want to save each book individually
                // this way we can access each book info. by it's _id
                for (var i = 0; i < restaurantes.length; i++) {
                    LSFactory.set(restaurantes[i].id, restaurantes[i]);
                };
            }

        }
    ])

    .controller('RestaurantCtrl', ['$scope', '$state', 'LSFactory', 'AuthFactory', '$rootScope', 'UserFactory', 'Loader',
        function ($scope, $state, LSFactory, AuthFactory, $rootScope, UserFactory, Loader) {

            var restaurantId = $state.params.restaurantId;
            $scope.restaurant = LSFactory.get(restaurantId);

            $scope.$on('reservarMesa', function () {
                $state.go('app.restaurant_data_reserva', { "restaurantId": restaurantId });
            });

            $scope.reservarMesa = function () {
                if (!AuthFactory.isLoggedIn()) {
                    $rootScope.$broadcast('showLoginModal', $scope, null, function () {
                        // user is now logged in
                        $scope.$broadcast('reservarMesa');
                    });
                    return;
                }
                $scope.$broadcast('reservarMesa');
            }
        }
    ])


    .controller('RestaurantDataReservaCtrl', ['$scope', '$state', 'LSFactory', 'RestaurantsFactory', 'AuthFactory', '$rootScope', 'UserFactory', 'Loader',
        function ($scope, $state, LSFactory, RestaurantsFactory, AuthFactory, $rootScope, UserFactory, Loader) {

            var restaurantId = $state.params.restaurantId;
            $scope.restaurant = LSFactory.get(restaurantId);

            $scope.escolheData = function (dataEscolhida) {
                RestaurantsFactory.getHorarios({
                    id: restaurantId,
                    data: dataEscolhida
                });
            }
            //.then(function success(), function error())
            
        }
    ])
    
 
    .controller('RestaurantHorarioReservaCtrl', ['$scope', '$state', 'LSFactory', 'AuthFactory', '$rootScope', 'UserFactory', 'Loader',
        //EDITAR
        function ($scope, $state, LSFactory, AuthFactory, $rootScope, UserFactory, Loader) {

            var restaurantId = $state.params.restaurantId;
            $scope.restaurant = LSFactory.get(restaurantId);

            $scope.$on('addToCart', function () {
                Loader.showLoading('Adding to Cart..');
                UserFactory.addToCart({
                    //criação do objeto reserva aqui...
                    id: restaurantId,
                    qty: 1
                }).success(function (data) {
                    Loader.hideLoading();
                    Loader.toggleLoadingWithMessage('Successfully added ' + $scope.book.title + ' to your cart', 2000);
                }).error(function (err, statusCode) {
                    Loader.hideLoading();
                    Loader.toggleLoadingWithMessage(err.message);
                });

            });

            $scope.addToCart = function () {
                if (!AuthFactory.isLoggedIn()) {
                    $rootScope.$broadcast('showLoginModal', $scope, null, function () {
                        // user is now logged in
                        $scope.$broadcast('addToCart');
                    });
                    return;
                }
                $scope.$broadcast('addToCart');
            }
        }
    ])
    
    .controller('RestaurantDataReservaCtrlPadrao', ['$scope', '$state', 'LSFactory', 'AuthFactory', '$rootScope', 'UserFactory', 'Loader',
        //EDITAR
        function ($scope, $state, LSFactory, AuthFactory, $rootScope, UserFactory, Loader) {

            var restaurantId = $state.params.restaurantId;
            $scope.restaurant = LSFactory.get(restaurantId);

            $scope.$on('addToCart', function () {
                Loader.showLoading('Adding to Cart..');
                UserFactory.addToCart({
                    //criação do objeto reserva aqui...
                    id: restaurantId,
                    qty: 1
                }).success(function (data) {
                    Loader.hideLoading();
                    Loader.toggleLoadingWithMessage('Successfully added ' + $scope.book.title + ' to your cart', 2000);
                }).error(function (err, statusCode) {
                    Loader.hideLoading();
                    Loader.toggleLoadingWithMessage(err.message);
                });

            });

            $scope.addToCart = function () {
                if (!AuthFactory.isLoggedIn()) {
                    $rootScope.$broadcast('showLoginModal', $scope, null, function () {
                        // user is now logged in
                        $scope.$broadcast('addToCart');
                    });
                    return;
                }
                $scope.$broadcast('addToCart');
            }
        }
    ])




    .controller('MinhasReservasCtrl', ['$scope', 'AuthFactory', '$rootScope', '$location', '$timeout', 'UserFactory', 'Loader',
        function ($scope, AuthFactory, $rootScope, $location, $timeout, UserFactory, Loader) {

            $scope.$on('getCart', function () {
                Loader.showLoading('Fetching Your Cart..');
                UserFactory.getCartItems().success(function (data) {
                    $scope.books = data.data;
                    Loader.hideLoading();
                }).error(function (err, statusCode) {
                    Loader.hideLoading();
                    Loader.toggleLoadingWithMessage(err.message);
                });
            });

            if (!AuthFactory.isLoggedIn()) {
                $rootScope.$broadcast('showLoginModal', $scope, function () {
                    // cancel auth callback
                    $timeout(function () {
                        $location.path('/app/browse');
                    }, 200);
                }, function () {
                    // user is now logged in
                    $scope.$broadcast('getCart');
                });
                return;
            }

            $scope.$broadcast('getCart');

            $scope.checkout = function () {
                // we need to send only the id and qty
                var _cart = $scope.books;
                var cart = [];
                for (var i = 0; i < _cart.length; i++) {
                    cart.push({
                        id: _cart[i]._id,
                        qty: 1 // hardcoded to 1
                    });
                };

                Loader.showLoading('Checking out..');
                UserFactory.addPurchase(cart).success(function (data) {
                    Loader.hideLoading();
                    Loader.toggleLoadingWithMessage('Successfully checked out', 2000);
                    $scope.books = [];
                }).error(function (err, statusCode) {
                    Loader.hideLoading();
                    Loader.toggleLoadingWithMessage(err.message);
                });
            }
        }
    ])


