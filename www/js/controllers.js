angular.module('ReserveApp.controllers', [])

    .controller('AppCtrl', ['$rootScope', '$ionicModal', 'AuthFactory', '$location', 'UserFactory', '$scope', 'Loader',
        function ($rootScope, $ionicModal, AuthFactory, $location, UserFactory, $scope, Loader) {

            $rootScope.$on('showLoginModal', function ($event, scope, cancelCallback, callback) {
                $scope.user = {
                    email: '',
                    password: ''
                };

                $scope = scope || $scope;

                $scope.viewLogin = true;

                $ionicModal.fromTemplateUrl('templates/login.html', {
                    scope: $scope
                }).then(function (modal) {
                    $scope.modal = modal;
                    $scope.modal.show();

                    $scope.switchTab = function (tab) {
                        if (tab === 'login') {
                            $scope.viewLogin = true;
                        } else {
                            $scope.viewLogin = false;
                        }
                    }

                    $scope.hide = function () {
                        $scope.modal.hide();
                        if (typeof cancelCallback === 'function') {
                            cancelCallback();
                        }
                    }

                    $scope.login = function () {
                        Loader.showLoading('Authenticating...');

                        UserFactory.login($scope.user).success(function (data) {

                            data = data.data;
                            AuthFactory.setUser(data.user);
                            AuthFactory.setToken({
                                token: data.token,
                                expires: data.expires
                            });

                            $rootScope.isAuthenticated = true;
                            $scope.modal.hide();
                            Loader.hideLoading();
                            if (typeof callback === 'function') {
                                callback();
                            }
                        }).error(function (err, statusCode) {
                            Loader.hideLoading();
                            Loader.toggleLoadingWithMessage(err.message);
                        });
                    }

                    $scope.register = function () {
                        Loader.showLoading('Registering...');

                        UserFactory.register($scope.user).success(function (data) {

                            data = data.data;
                            AuthFactory.setUser(data.user);
                            AuthFactory.setToken({
                                token: data.token,
                                expires: data.expires
                            });

                            $rootScope.isAuthenticated = true;
                            Loader.hideLoading();
                            $scope.modal.hide();
                            if (typeof callback === 'function') {
                                callback();
                            }
                        }).error(function (err, statusCode) {
                            Loader.hideLoading();
                            Loader.toggleLoadingWithMessage(err.message);
                        });
                    }
                });
            });

            $rootScope.loginFromMenu = function () {
                $rootScope.$broadcast('showLoginModal', $scope, null, null);
            }

            $rootScope.logout = function () {
                UserFactory.logout();
                $rootScope.isAuthenticated = false;
                $location.path('/app/browse');
                Loader.toggleLoadingWithMessage('Successfully Logged Out!', 2000);
            }


        }
    ])

    .controller('BrowseCtrl', ['$scope', 'RestaurantsFactory', 'LSFactory', 'Loader',
        function ($scope, RestaurantsFactory, LSFactory, Loader) {

            Loader.showLoading();

            $scope.restaurants = [];
            var restaurants = LSFactory.getAll();

            // if restaurants exists in localStorage, use that instead of making a call
            if (restaurants.length > 0) {
                $scope.restaurants = restaurants;
                Loader.hideLoading();
            } else {
                RestaurantsFactory.get().success(function (restaurantes) {
                    // process restaurants and store them 
                    // in localStorage so we can work with them later on, 
                    // when the user is offline
                    console.log(restaurantes[0].nome)
                    processRestaurants(restaurantes);
                    $scope.restaurants = restaurantes;
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                    Loader.hideLoading();
                }).error(function (err, statusCode) {
                    Loader.hideLoading();
                    Loader.toggleLoadingWithMessage(err.message);
                });
            }

            function processRestaurants(restaurantes) {
                LSFactory.clear();
                // we want to save each restaurant individually
                // this way we can access each restaurant info. by it's .id
                for (var i = 0; i < restaurantes.length; i++) {
                    LSFactory.set(restaurantes[i].id, restaurantes[i]);
                };
            }
        }
    ])

    .controller('RestaurantCtrl', ['$scope', '$state', 'LSFactory', 'AuthFactory', '$rootScope', 'UserFactory', 'Loader',
        function ($scope, $state, LSFactory, AuthFactory, $rootScope, UserFactory, Loader) {

            var restaurantId = $state.params.restaurantId;
            $scope.restaurant = LSFactory.get(restaurantId);

            $scope.$on('reservarMesa', function () {
                $state.go('app.restaurant_data_reserva', { "restaurantId": restaurantId });
            });

            $scope.reservarMesa = function () {
                if (!AuthFactory.isLoggedIn()) {
                    $rootScope.$broadcast('showLoginModal', $scope, null, function () {
                        $scope.$broadcast('reservarMesa');
                    });
                    return;
                }
                $scope.$broadcast('reservarMesa');
            }
        }
    ])
    
    .controller('RestaurantDataReservaFakeCtrl', ['$scope', '$state', 'LSFactory', 'AuthFactory', '$rootScope', 'UserFactory', 'Loader',
        function ($scope, $state, LSFactory, AuthFactory, $rootScope, UserFactory, Loader) {
            var restaurantId = $state.params.restaurantId;
            $scope.restaurant = LSFactory.get(restaurantId);

            $scope.escolheData = function (dataEscolhida) {
                $state.go('app.restaurant_horario_reserva', { "restaurantId": restaurantId });
            }
        }
    ])

    .controller('RestaurantHorarioReservaFakeCtrl', ['$scope', '$state', 'LSFactory', 'AuthFactory', '$rootScope', 'UserFactory', 'Loader',
        function ($scope, $state, LSFactory, AuthFactory, $rootScope, UserFactory, Loader) {
            var restaurantId = $state.params.restaurantId;
            $scope.restaurant = LSFactory.get(restaurantId);

            $scope.escolheHorario = function (horarioEscolhido) {
                $state.go('app.reserva_dados_cliente', { "restaurantId": restaurantId });
            }
        }
    ])
    
    .controller('ReservaDadosClienteFakeCtrl', ['$scope', '$state', 'LSFactory', 'AuthFactory', '$rootScope', 'UserFactory', 'Loader',
        function ($scope, $state, LSFactory, AuthFactory, $rootScope, UserFactory, Loader) {
            var restaurantId = $state.params.restaurantId;
            $scope.restaurant = LSFactory.get(restaurantId);

            $scope.enviarDadosCliente = function (nomeCliente,emailCliente,telefoneCliente) {
                $state.go('app.browse');
            }
        }
    ])

    .controller('RestaurantDataReservaCtrl', ['$scope', '$state', 'LSFactory', 'RestaurantsFactory', 'AuthFactory', '$rootScope', 'UserFactory', 'Loader',
        function ($scope, $state, LSFactory, RestaurantsFactory, AuthFactory, $rootScope, UserFactory, Loader) {

            var restaurantId = $state.params.restaurantId;
            $scope.restaurant = LSFactory.get(restaurantId);

            $scope.escolheData = function (dataEscolhida) {
                RestaurantsFactory.getHorarios({
                    id: restaurantId,
                    data: dataEscolhida
                }).success(function (data) {
                    Loader.toggleLoadingWithMessage('Data escolhida com sucesso');
                }).error(function (err, statusCode) {
                    Loader.hideLoading();
                    Loader.toggleLoadingWithMessage(err.message);
                });
            }
        }
    ])

     .controller('MinhasReservasCtrl', ['$scope', 'AuthFactory', '$rootScope', '$location', '$timeout', 'UserFactory', 'Loader',
        function ($scope, AuthFactory, $rootScope, $location, $timeout, UserFactory, Loader) {
            //TODO            
        }
    ])


