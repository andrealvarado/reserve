angular.module('ReserveApp', ['ionic', 'ReserveApp.controllers', 'ReserveApp.factory'])
    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
    }).run(['$rootScope', 'AuthFactory',
        function ($rootScope, AuthFactory) {

            $rootScope.isAuthenticated = AuthFactory.isLoggedIn();

            $rootScope.getNumber = function (num) {
                return new Array(num);
            }

        }
    ]).config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
        function ($stateProvider, $urlRouterProvider, $httpProvider) {

            $httpProvider.interceptors.push('TokenInterceptor');

            $stateProvider
                .state('app', {
                    url: "/app",
                    abstract: true,
                    templateUrl: "templates/menu.html",
                    controller: 'AppCtrl'
                })

                .state('app.browse', {
                    url: "/browse",
                    views: {
                        'menuContent': {
                            templateUrl: "templates/browse.html",
                            controller: 'BrowseCtrl'
                        }
                    }
                })

                .state('app.restaurant', {
                    url: "/restaurant/:restaurantId",
                    views: {
                        'menuContent': {
                            templateUrl: "templates/restaurant.html",
                            controller: 'RestaurantCtrl'
                        }
                    }
                })
                
                .state('app.restaurant_data_reserva', {
                    url: "/restaurant_data_reserva/:restaurantId",
                    views: {
                        'menuContent': {
                            templateUrl: "templates/restaurant_data_reserva.html",
                            controller: 'RestaurantDataReservaFakeCtrl'
                            //controller: 'RestaurantDataReservaCtrl'
                        }
                    }
                })
                .state('app.restaurant_horario_reserva', {
                    url: "/restaurant_horario_reserva/:restaurantId",
                    views: {
                        'menuContent': {
                            templateUrl: "templates/restaurant_horario_reserva.html",
                            controller: 'RestaurantHorarioReservaFakeCtrl'
                            //controller: 'RestaurantHorarioReservaCtrl'
                        }
                    }
                })
                .state('app.reserva_dados_cliente', {
                    url: "/reserva_dados_cliente/:restaurantId",
                    views: {
                        'menuContent': {
                            templateUrl: "templates/reserva_dados_cliente.html",
                            controller: 'ReservaDadosClienteFakeCtrl'
                            //controller: 'ReservaDadosClienteCtrl'
                        }
                    }
                })
                
                .state('app.minhas_reservas', {
                    url: "/minhas_reservas",
                    views: {
                        'menuContent': {
                            templateUrl: "templates/minhas_reservas.html",
                            controller: 'MinhasReservasCtrl'
                        }
                    }
                })

            // if none of the above states are matched, use this as the fallback
            $urlRouterProvider.otherwise('/app/browse');
        }
    ])

